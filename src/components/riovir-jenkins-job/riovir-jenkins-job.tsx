import { Component, Prop, State, Method, PropDidChange } from '@stencil/core';

interface Job {
    building: boolean;
    fullDisplayName: string;
    timestamp: number;
    estimatedDuration: number;
    result: string;
}

@Component({
  tag: 'riovir-jenkins-job'
})
export class JenkinsJob {
  @State() data: Job;

  @Prop() src: string;
  @Prop() pollSecond: number = 0;

  // Semantic props
  @Prop() inverted: boolean;
  @Prop() tiny: boolean;
  @Prop() small: boolean;
  @Prop() large: boolean;
  @Prop() big: boolean;

  fetchTask: any;

  componentWillLoad() {
    this.scheduleFetch();
  }

  scheduleFetch() {
    this.fetchData();
    if (this.pollSecond <= 0) { return; }
    this.fetchTask = window.setTimeout(() => {
      this.scheduleFetch();
    }, this.pollSecond * 1000);
  }

  @PropDidChange('src')
  handleSrc() { this.fetchData(); }

  @PropDidChange('pollSecond')
  handlePollSecond() {
    if(this.fetchTask) {
      window.clearTimeout(this.fetchTask);
      this.fetchTask = null;
    }
    if (this.pollSecond > 0) { this.scheduleFetch(); }
  }

  @Method()
  async fetchData() {
    try {
      const response = await window.fetch(this.src);
      this.data = await response.json();
    }
    catch(err) {
      console.error(`Unable to get job result from ${this.src}`, err);
    }
  }

  private calculateProgress = (job: Job): number => {
    return job.building ? Math.min((Date.now() - job.timestamp) / job.estimatedDuration * 100, 100) : 100;
  }

  render() {
    if (!this.data) { return (<div>...</div>); }
    const job = this.data;

    const percentage = Math.round(this.calculateProgress(job));
    const progressClasses = {
      ui: true,
      progress: true,
      inverted: this.inverted,
      tiny: this.tiny,
      small: this.small,
      large: this.large,
      big: this.big,
      active:  job.building,
      success: job.result === 'SUCCESS',
      warning: job.result === 'UNSTABLE',
      error: job.result === 'FAILURE'
    };

    return (<div class={progressClasses} data-percent={percentage}>
      <div class="bar" style={{ 'transition-duration': '300ms', width: `${percentage}%` }}>
        <div class="progress">{percentage}%</div>
      </div>
      <div class="label">{this.data.fullDisplayName}</div>
    </div>);
  }
}
