import { Component, Prop, State, Method, PropDidChange } from '@stencil/core';

@Component({
  tag: 'riovir-metric',
  styleUrl: 'riovir-metric.scss',
  shadow: true
})
export class Metric {
  @State() data: object;
  @Prop() src: string;
  @Prop() field: string;
  @Prop() pollSecond: number = 0;

  fetchTask: any;

  componentWillLoad() {
    this.scheduleFetch();
  }

  scheduleFetch() {
    this.fetchData();
    if (this.pollSecond <= 0) { return; }
    this.fetchTask = window.setTimeout(() => {
      this.scheduleFetch();
    }, this.pollSecond * 1000);
  }

  @PropDidChange('src')
  handleSrc() { this.fetchData(); }

  @PropDidChange('pollSecond')
  handlePollSecond() {
    if(this.fetchTask) {
      window.clearTimeout(this.fetchTask);
      this.fetchTask = null;
    }
    if (this.pollSecond > 0) { this.scheduleFetch(); }
  }

  @Method()
  async fetchData() {
    const response = await window.fetch(this.src);
    this.data = await response.json();
  }

  render() {
    const formatAll = data => Object.keys(data)
        .map(key => <div>{key}: {JSON.stringify(data[key])}</div>);
    const formatField = data => {
      const fields = this.field.split('.');
      const value = fields.reduce((val, key) => val[key], data);
      return <div>{value}</div>
    };
    const format = this.field ? formatField : formatAll;
    return this.data
      ? (<div>{format(this.data)}</div>)
      : (<div>Waiting for data</div>)
  }
}
