import { Component, Prop } from '@stencil/core';

@Component({
  tag: 'riovir-icon'
})
export class UseIcon {
  @Prop() name: string;

  @Prop() size: string;
  @Prop() customWidth: string;
  @Prop() customHeight: string;

  render() {
    const width = this.customWidth || this.size || 32;
    const height = this.customHeight || this.size || 32;

    const sizedRef = `#${this.name}-${height}`;
    const sizedIcon = document.querySelector(sizedRef);
    const ref = sizedIcon ? sizedRef : `#${this.name}`;
    return <svg width={width} height={height} aria-hidden="true">
      <use href={ref} />
      Sorry, your browser does not support inline SVG.
    </svg>
  }
}
